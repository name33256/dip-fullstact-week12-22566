import { createRouter,createWebHistory } from "vue-router";
import HomePage from "../views/HomePage.vue" ;
// import AboutPage from "../Views/AboutPage.vue";
import NotFound from '@/views/NotFound.vue';

const router = createRouter({
  history: createWebHistory(),
  router: [
        {
          path:'/',
          name: 'home',
          component:()=>import('../views/HomePage.vue'),
        },
        {
          path: "/about",
          name:'about',
          component:()=> import('../views/AboutPage.vue'),
        },
        {
          path: "/user",
          name:'user',
          component:()=> import('../views/UserPage.vue'),
        },
        {
          path: "/user/:id",
          name:"userSingle",
          component:()=> import('../views/UserSinglePage.vue'),
          props:true
        },
        {
          path: '/member',
          redirect:'/about',
        },
        {
          path: "/:pathMatch(.*)*",
          name: 'notfound',
          component: Notfound,
        }
    ],
});
export default router;